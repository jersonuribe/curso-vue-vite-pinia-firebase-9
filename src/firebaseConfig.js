// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth'


const firebaseConfig = {
    apiKey: "AIzaSyCTc3Ebv8jyb6Z1CaucvOHay0wjURj4Q3k",
    authDomain: "vue-3-2022-db6ad.firebaseapp.com",
    projectId: "vue-3-2022-db6ad",
    storageBucket: "vue-3-2022-db6ad.appspot.com",
    messagingSenderId: "608552707267",
    appId: "1:608552707267:web:61d03ebc577cb7f3482e76"
};

initializeApp(firebaseConfig);

const auth = getAuth();

export { auth };